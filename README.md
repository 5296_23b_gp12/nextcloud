This artifact is mainly in three parts:

- Benchmarking NextCloud instances on different EC2 instances, with an instruction guide
    - available at [Instruction Guide](/benchmark_instruction.pdf)
- Stress test show cases using
    - [Lighthouse](https://drive.google.com/file/d/1PuSxqh7h5m-fJIgXRLDBnYjpzN7RFFpW/view?usp=share_link)
    - [Virtual User Generator](https://drive.google.com/file/d/1l6SFz-CFoafLbTq4x4YasdgJ0mTfVJfm/view?usp=share_link)
    - [Apache JMeter](https://drive.google.com/file/d/1nchoysD8LaPDYBYnOOrMmFG8piQ5qeSo/view?usp=share_link)
- A recommendaton setup for NextCloud.


## Recommendation setup for NextCloud ##

**Prerequisites:**
- Choosean EC2 (at least medium type) and Ubuntu 22.04 image
- Install Docker and Docker Compose, following the instructions provided in the links below
    - [Installation guide for Docker](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-22-04)
    - [Installation guide for Docker Compose](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-22-04)
- A registered domain name (e.g. your-domain.com)
- Pull this repository `git clone git@gitlab.com:5296_23b_gp12/nextcloud.git` to EC2 instance
- Creation of a .env file in root directory containing variables as outlined in the .env.sample
- Enabled inbound port 80 and 443 in the EC2 security rules


**Instructions:**\
To run this NextCloud instance using Docker Compose, execute the following command:\
`docker compose up -d`

To view current running containers, use the following command:\
`docker ps`

The startup process takes around 5-10 mins to obtain SSL certificate and initialize the NextCloud instance.
Once the process is completed, you can visit the NextCloud instance directly in your browser (e.g. https://your-domain.com)

NextCloud also provides an online security scanner to check any vulnerability of your NextCloud instances, you can input your NextCloud URL to use this scanner
[NextCloud Scanner](https://scan.nextcloud.com/)


